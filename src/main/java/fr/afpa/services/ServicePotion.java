package fr.afpa.services;

import fr.afpa.entites.Joueur;
import fr.afpa.entites.PotionForce;
import fr.afpa.entites.PotionVie;
import fr.afpa.entites.Potions;
import fr.afpa.outils.Outils;

public class ServicePotion {

	/**
	 * Methode qui genere une potions en fonction du nombre entree avec les points
	 * aleatoires
	 * 
	 * @return une potions crée aleatoirement
	 */
	public Potions generationPotionAlea() {
		
		int a = Outils.generationAlea(3);
		Potions potionAlea = null;

		if (a == 1) {
			potionAlea = new PotionVie();

		}
		if (a == 2) {

			potionAlea = new PotionForce();
		}
		return potionAlea;
	}

	/**
	 * Methode pour utiliser la potion, en fonction de la potion ce sont les points
	 * de vie ou les points de force qui seront augmentés
	 * 
	 * @param potion : La potion generée aléatirement
	 * @param joueur : Le personnage joueur sur le quel vont s'appliquer les
	 *               modifications
	 * @return un boolean si l'augmentations des attributs du joueur à bien ete
	 *         effectue;
	 */
	public int utiliserPotion(Potions potion, Joueur joueur) {

		if (potion instanceof PotionVie) {
			joueur.setPointDeVie(joueur.getPointDeVie() + potion.getPointsPotion());
			return potion.getPointsPotion();
		} 
		else if (potion instanceof PotionForce) {
			joueur.setPointDeForce(joueur.getPointDeForce() + potion.getPointsPotion());
			return potion.getPointsPotion();
		} else
			return -1;
	}
}
