package fr.afpa.services;

import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;



import java.util.Map.Entry;

import fr.afpa.entites.Labyrinthe;
import fr.afpa.entites.Monstre;
import fr.afpa.entites.Salle;
import fr.afpa.outils.Outils;

public class MonstreService {
	
	/**
	 * Methode qui permet de generer un monstre 
	 * @return le monstre generé avec des points de vie et de force en fonction de la taille du labyrinthe
	 */
	public  Monstre generationMonstre(Labyrinthe laby) {
		
		
			int pv = laby.getLongueur()*Outils.generationAleaMinMAx(4, 6);
			int pf = Outils.generationAleaMinMAx(laby.getLargeur(), laby.getLargeur()*2);
			Monstre monstreCree = new Monstre(pv, pf);

			return monstreCree;
		 

	}
	
	/**
	 * Fonction permettant de donner à chaque salle une position à chaque monstre
	 * @param laby
	 */
	public void donnerPositionMonstre(Labyrinthe laby) {
		Salle[][] sallesT2 = laby.getSalles();
		for (Salle[] salleT1 : sallesT2)  {
			for (Salle salle : salleT1) {
				if(salle.getListeMonstres().size()!=0) {
					 for (Entry<String, Monstre> entry : salle.getListeMonstres().entrySet()) {
					     entry.getValue().setPositionX(salle.getPosX());
					 	 entry.getValue().setPositionY(salle.getPosY());
					 }
				}
			}
		}
	}
	
	/**
	 * fonction permettant de gérer aléatoire les déplacements du monstre
	 * @param laby
	 */
	public static void deplacementAleatoireMonstre(Labyrinthe laby) {
		for(int i=0;i<laby.getSalles().length;i++) {
			for(int j=0;j<laby.getSalles().length;j++) {
				if(laby.getSalles()[i][j].getListeMonstres().size()!=0) {	
					
					Iterator<Entry<String, Monstre>> it = laby.getSalles()[i][j].getListeMonstres().entrySet().iterator();
					while (it.hasNext()) {
						int[] tab = direction(laby, i, j);
						if(tab[2] == 0 && laby.getSalles()[i][j].isNotMurNord() && !laby.getSalles()[tab[0]][tab[1]].isOut() && laby.getSalles()[tab[0]][tab[1]] != laby.getSalles()[0][0]) {
						
							Map.Entry<String, Monstre> listeMonstre = (Map.Entry<String, Monstre>) it.next();
							laby.getSalles()[tab[0]][tab[1]].getListeMonstres().put(listeMonstre.getKey(), listeMonstre.getValue());
							it.remove();
						}
						
						if(tab[2] == 1 && laby.getSalles()[i][j].isNotMurSud() && !laby.getSalles()[tab[0]][tab[1]].isOut() && laby.getSalles()[tab[0]][tab[1]] != laby.getSalles()[0][0]) {
							Map.Entry<String, Monstre> listeMonstre = (Map.Entry<String, Monstre>) it.next();
							laby.getSalles()[tab[0]][tab[1]].getListeMonstres().put(listeMonstre.getKey(), listeMonstre.getValue());
							it.remove();
						}
						if(tab[2] == 2 && laby.getSalles()[i][j].isNotMurEst() && !laby.getSalles()[tab[0]][tab[1]].isOut() && laby.getSalles()[tab[0]][tab[1]] != laby.getSalles()[0][0]) {
							
							
							Map.Entry<String, Monstre> listeMonstre = (Map.Entry<String, Monstre>) it.next();
							laby.getSalles()[tab[0]][tab[1]].getListeMonstres().put(listeMonstre.getKey(), listeMonstre.getValue());
							it.remove();
					
						}
						if(tab[2] == 3 && laby.getSalles()[i][j].isNotMurOuest() && !laby.getSalles()[tab[0]][tab[1]].isOut() && laby.getSalles()[tab[0]][tab[1]] != laby.getSalles()[0][0]) {		
							Map.Entry<String, Monstre> listeMonstre = (Map.Entry<String, Monstre>) it.next();
							laby.getSalles()[tab[0]][tab[1]].getListeMonstres().put(listeMonstre.getKey(), listeMonstre.getValue());
							it.remove();				
						}
					}	
				}										
			}
		}
	}
	
	/**
	 * Choisir la direction aléatoire des monstres
	 * @param laby
	 * @param posXActuel
	 * @param posYActuel
	 * @return
	 */
	public static int[] direction(Labyrinthe laby, int posXActuel, int posYActuel) {
		int random = 0;
		int[] tabIndice = new int[3];
		do {
			random = new Random().nextInt(4);
			if(random == 0) { // nord
				int tmpPosXActuel = posXActuel - 1 ;
				int tmpPosYActuel = posYActuel + 0;
				if(LabyrintheService.controlPositionActuel(laby, tmpPosXActuel, tmpPosYActuel)) {
						tabIndice[0] = tmpPosXActuel;
						tabIndice[1] = tmpPosYActuel;
						tabIndice[2] = 0; // nord
						return tabIndice;
					}
				}			
			
			if(random == 1) { // sud
				int tmpPosXActuel = posXActuel + 1;
				int tmpPosYActuel = posYActuel + 0;
				if(LabyrintheService.controlPositionActuel(laby, tmpPosXActuel, tmpPosYActuel)) {					
						tabIndice[0] = tmpPosXActuel;
						tabIndice[1] = tmpPosYActuel;
						tabIndice[2] = 1; // sud
						return tabIndice;
				}
			}
			if(random == 2) { // est
				int tmpPosXActuel = posXActuel + 0;
				int tmpPosYActuel = posYActuel + 1;
				if(LabyrintheService.controlPositionActuel(laby, tmpPosXActuel, tmpPosYActuel)) {
						tabIndice[0] = tmpPosXActuel;
						tabIndice[1] = tmpPosYActuel;
						tabIndice[2] = 2; // est
						return tabIndice;
					
				}			
			}
			if(random == 3) { // ouest
				int tmpPosXActuel = posXActuel + 0;
				int tmpPosYActuel = posYActuel - 1;
				if(LabyrintheService.controlPositionActuel(laby, tmpPosXActuel, tmpPosYActuel)) {
			
						tabIndice[0] = tmpPosXActuel;
						tabIndice[1] = tmpPosYActuel;
						tabIndice[2] = 3; // ouest
						return tabIndice;
					}
			}

		}while(true);
		
	}


}
