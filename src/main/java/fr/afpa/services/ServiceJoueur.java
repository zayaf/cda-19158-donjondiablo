package fr.afpa.services;

import fr.afpa.entites.Joueur;
import fr.afpa.entites.Labyrinthe;

public class ServiceJoueur {
	
	/**
	 * Creation du joueur
	 * @param laby
	 * @param nom
	 * @return un joueur
	 */
	public Joueur createJoueur(Labyrinthe laby,String nom) {
		boolean nomValid = false;
		Joueur joueur = null;
		while(!nomValid){
			if(nom != null && nom != "") {
			    joueur = new Joueur(laby.getLongueur()*5, laby.getLongueur()*2, 5, nom);
				nomValid = true;
			}
			else {
				System.out.println("Votre nom est invalide");
			}
		}
		return joueur;	
	}
	
	/**
	 * permet d'accorder des points de vie de depart au joueur
	 * @param laby
	 * @return les points de vie pour commencer le jeu
	 */
	public static  int pointDevieJoeurDebut(Labyrinthe laby) {
		
		
		int pointDevie = laby.getLargeur() *(5);
		
		
		return pointDevie;
		
	}


	/**
	 * permet d'accorder des points de force de depart au joueur
	 * @param laby
	 * @return les points de force pour commencer le jeu
	 */
public static  int pointDeForceJoeurDebut(Labyrinthe laby) {
		
		
		int pointDeForce = laby.getLargeur() *(3);
		
		
		return pointDeForce;
		
	}

}
