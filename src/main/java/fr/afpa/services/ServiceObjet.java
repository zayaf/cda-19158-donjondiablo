package fr.afpa.services;

import fr.afpa.entites.Bandit;
import fr.afpa.entites.BourseOr;
import fr.afpa.entites.Joueur;
import fr.afpa.entites.Objet;
import fr.afpa.entites.PotionForce;
import fr.afpa.entites.PotionVie;
import fr.afpa.entites.Potions;
import fr.afpa.outils.Outils;

public class ServiceObjet {

	/**
	 * Methode de generation aleatoire d'objets
	 * 
	 * @param ServicePotion: appele le service de potions pour pouvoir créer des
	 *                       potions aleatoires
	 * @return Objet : soit une potion de vie ou de force, soit un bandit manchot
	 *         soit, une bourse
	 */
	public Objet generationObjet(ServicePotion servPotion) {

		int n = Outils.generationAlea(5);
		Objet objetCree = null;

		switch (n) {
		case 1:
			objetCree = new PotionVie();
			return objetCree;
		case 2:
			objetCree = new PotionForce();
			return objetCree;
		case 3:
			objetCree = new BourseOr();
			return objetCree;
		case 4:
			objetCree = new Bandit();
			return objetCree;
		}
		return objetCree;
	}

	/**
	 * Methode qui prend en parametres le joueur et le bandit manchot trouvé dans la
	 * salle, l'or du joueur est comparé à celui du bandit. Le service potion est
	 * aussi appele pr la créetion des potions
	 * 
	 * @param joueur     : joueur, pour pouvoir comparer l'or
	 * @param bandit:    pour comparer l'or demandée avec l'or du joueur
	 * @param servPotion : pour appeler la methode de création de potions
	 * @return une potion;
	 */
	public Potions utilisbanditManchot(Joueur joueur, Bandit bandit, ServicePotion servPotion) {
		Potions potion;

		if (joueur.getOr() >= bandit.getOrDemande()) {
			joueur.setOr(joueur.getOr() - bandit.getOrDemande());
			potion = servPotion.generationPotionAlea();

			return potion;

		} else {
			return null;
		}

	}

	/**
	 * Methode pour faire rammasser la bourse au joueur une fois fait, la bourse
	 * passe à faux.
	 * 
	 * @param joueur : Joueur qui ramasse la bourse
	 * @param bourse : Bourse ramassée
	 * @return un entier pour afficher les pieces d'or gagnés. ATTENTION renvoit -1
	 *         si un ou deux parametre est null
	 */
	public int ramasserBourse(Joueur joueur, BourseOr bourse) {
		int temp = bourse.getOrBourse();

		if (joueur != null && bourse != null) {
			joueur.setOr(joueur.getOr() + bourse.getOrBourse());
			return temp;
		} else
			return -1;
	}

}
