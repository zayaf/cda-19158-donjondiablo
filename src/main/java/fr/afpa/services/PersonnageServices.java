package fr.afpa.services;

import java.util.Scanner;

import fr.afpa.entites.Labyrinthe;
import fr.afpa.entites.Personnage;
import fr.afpa.entites.Salle;
import fr.afpa.ihm.JoueurIhm;
import fr.afpa.ihm.LabyrintheIhm;
import fr.afpa.ihm.SalleIhm;
import fr.afpa.outils.Outils;

public class PersonnageServices {

	/**
	 * Methode qui gere l'attaque d'un personnage. ATTENTION a bien respecter
	 * l'ordre celui qui attaque doit etre entrée en parametres avant, suivie du
	 * personnage attaqué
	 * 
	 * @param quiAttaque : le personnage qui attaque
	 * @param estAttaque : le personnage attaque
	 * @return un int: le nombre de vie perdus si les Personnages entrée en
	 *         parametre sont non null; si nulls il renvoit -1
	 */
	public int attaquer(Personnage quiAttaque, Personnage estAttaque) {
		if (quiAttaque != null && estAttaque != null) {
			estAttaque.setPointDeVie(estAttaque.getPointDeVie() - quiAttaque.getPointDeForce());
			return quiAttaque.getPointDeForce();
		} else
			return -1;
	}

	/**
	 * permet d'afficher la ou les directions possibles en fonction des murs
	 * presents ou pas
	 * 
	 * @param salle
	 */
	public void choixDirection(Salle salle) {

		System.out.println("Quelle direction voulez-vous prendre?");

		if (salle.isJoueurPresent() && salle.isNotMurNord()) {
			System.out.println("1-Nord");
		}
		if (salle.isJoueurPresent() && salle.isNotMurSud()) {
			System.out.println("2-Sud");
		}
		if (salle.isJoueurPresent() && salle.isNotMurEst()) {
			System.out.println("3-Est");
		}
		if (salle.isJoueurPresent() && salle.isNotMurOuest()) {
			System.out.println("4-Ouest");
		}
		System.out.println("5-Retour");
	}

	public void menuDirection(Labyrinthe laby, Personnage personnage, Scanner in, ServiceSalle servSalle, JoueurIhm ihmJoueur, PersonnageServices perServ, SalleIhm salleIhm,
			ServicePotion servPotion, ServiceObjet servObj) {
		boolean quitter = false;
		String choix = "";

		while (!quitter) {
			choixDirection(laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()]);
			choix = in.nextLine();
			Outils.cls();
			switch (choix) {
			case "1":
				if (laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].isNotMurNord()) {
					laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].setJoueurPresent(false);
					laby.getSalles()[personnage.getPositionX() - 1][personnage.getPositionY()].setJoueurPresent(true);
					personnage.setPositionX(personnage.getPositionX() - 1);
					personnage.setPositionY(personnage.getPositionY());
				} 
					quitter=true;
			
				break;

			case "2":
				if (laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].isNotMurSud()) {
					laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].setJoueurPresent(false);
					laby.getSalles()[personnage.getPositionX() + 1][personnage.getPositionY()].setJoueurPresent(true);
					personnage.setPositionX(personnage.getPositionX() + 1);
					personnage.setPositionY(personnage.getPositionY());		
				} 
					quitter=true;
				break;

			case "3":
				if (laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].isNotMurEst()) {
					laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].setJoueurPresent(false);
					laby.getSalles()[personnage.getPositionX()][personnage.getPositionY() + 1].setJoueurPresent(true);
					personnage.setPositionX(personnage.getPositionX());
					personnage.setPositionY(personnage.getPositionY() + 1);
				} 

				quitter=true;
				break;
			case "4":
				if (laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].isNotMurOuest()) {
					laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].setJoueurPresent(false);
					laby.getSalles()[personnage.getPositionX()][personnage.getPositionY() - 1].setJoueurPresent(true);
					personnage.setPositionX(personnage.getPositionX());
					personnage.setPositionY(personnage.getPositionY() - 1);
				} 
					quitter=true;
				break;
			case "5":
				quitter = true;
				break;

			default:
				System.out.println("Erreur !");
			}

		}
	}
}
