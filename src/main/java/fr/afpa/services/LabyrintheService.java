package fr.afpa.services;

import java.util.Random;

import fr.afpa.entites.Joueur;
import fr.afpa.entites.Labyrinthe;
import fr.afpa.entites.Salle;
import fr.afpa.ihm.JoueurIhm;
import fr.afpa.outils.Outils;

public class LabyrintheService {
	
	/**
	 * Generation du labyrinthe et ses salles, incrémentation d'un compteur de salle visité
	 * Algorithme du chemin grace à la fonction recursive generateLabyRecursive
	 * @param longueur la longueur du laby
	 * @param largeur la largeur du laby
	 * @return un labyrinthe
	 */
	public Labyrinthe genereLabyrinthe(int longueur, int largeur, ServiceSalle servSalle, ServiceObjet servObj,ServicePotion servPotion, ServiceJoueur servJoueur) {
		Labyrinthe laby = new Labyrinthe(longueur, largeur);
		Joueur joueur = JoueurIhm.createJoueur(laby, servJoueur);
		laby.setJoueur(joueur);
		ServiceSalle ss = new ServiceSalle();
		ss.createSalle(laby);
		Salle.setNbvisite(1);
		laby.getSalles()[0][0].setNbrevisite(Salle.getNbvisite());
		laby.getSalles()[0][0].setSalleVisite(true);
		laby.getSalles()[0][0].setJoueurPresent(true);
		generateLabyRecursive(laby);
		Salle salle = sortie(laby);
		salle.setOut(true);
		generationAleaMonstresObjetsParSalle(laby, servSalle, servObj, servPotion);
		return laby;
	}
	
	
	/**
	 * méthode qui créer un tableau de direction selon un chiffre aléatoire entre 0 et 3
	 * 0 = nord, 1 = sud, 2 = est, 3 = ouest 
	 * @param laby labyrinthe actuel
	 * @param posXActuel position x actuel pour le chemin
	 * @param posYActuel position y actuel pour le chemin
	 * @return un tableau de direction contenant la position x, y et le numéro random pour la direction
	 */
	public int[] direction(Labyrinthe laby, int posXActuel, int posYActuel) {
		int random = 0;
		int[] tabIndice = new int[3];
		boolean routeN = false;
		boolean routeS = false;
		boolean routeE = false;
		boolean routeO = false;
		do {
			random = new Random().nextInt(4);
			if(random == 0) { // nord
				int tmpPosXActuel = posXActuel - 1 ;
				int tmpPosYActuel = posYActuel + 0;
				if(controlPositionActuel(laby, tmpPosXActuel, tmpPosYActuel) && !laby.getSalles()[tmpPosXActuel][tmpPosYActuel].isSalleVisite()) {
						tabIndice[0] = tmpPosXActuel;
						tabIndice[1] = tmpPosYActuel;
						tabIndice[2] = 0; // nord
						return tabIndice;
					}else {
						routeN = true;
					}
				}			
			
			if(random == 1) { // sud
				int tmpPosXActuel = posXActuel + 1;
				int tmpPosYActuel = posYActuel + 0;
				if(controlPositionActuel(laby, tmpPosXActuel, tmpPosYActuel) && !laby.getSalles()[tmpPosXActuel][tmpPosYActuel].isSalleVisite()) {					
						tabIndice[0] = tmpPosXActuel;
						tabIndice[1] = tmpPosYActuel;
						tabIndice[2] = 1; // sud
						return tabIndice;
				}else {
					routeS = true;
				}
			}
			if(random == 2) { // est
				int tmpPosXActuel = posXActuel + 0;
				int tmpPosYActuel = posYActuel + 1;
				if(controlPositionActuel(laby, tmpPosXActuel, tmpPosYActuel) && !laby.getSalles()[tmpPosXActuel][tmpPosYActuel].isSalleVisite()) {
						tabIndice[0] = tmpPosXActuel;
						tabIndice[1] = tmpPosYActuel;
						tabIndice[2] = 2; // est
						return tabIndice;
					
				}else {
					routeE = true;
				}				
			}
			if(random == 3) { // ouest
				int tmpPosXActuel = posXActuel + 0;
				int tmpPosYActuel = posYActuel - 1;
				if(controlPositionActuel(laby, tmpPosXActuel, tmpPosYActuel) && !laby.getSalles()[tmpPosXActuel][tmpPosYActuel].isSalleVisite()) {
			
						tabIndice[0] = tmpPosXActuel;
						tabIndice[1] = tmpPosYActuel;
						tabIndice[2] = 3; // ouest
						return tabIndice;
					}
				else {
					routeO = true;
				}
			}
			
			if(routeN && routeS && routeE && routeO) {
				return tabIndice;
			}

		}while(true);
		
	}
	
	/**
	 * controle de la position actuel de la salle et vérifier qui ne se situe pas en dehors du laby
	 * @param laby
	 * @param posXActuel
	 * @param posYActuel
	 * @return vrai si le control se passe bien
	 */
	public static boolean controlPositionActuel(Labyrinthe laby,int posXActuel, int posYActuel) {
		if((posXActuel >= 0 && posXActuel < laby.getLongueur()) && (posYActuel >= 0 && posYActuel < laby.getLargeur())) {
			return true;
		}
		return false;
	}
	
	/**
	 * Selectionner un chemin aléatoire et lui affecter les nouvelles positions, et mettre la visite à true
	 * @param laby
	 * @return null si un chemin a déja été visité (pour la récursivité)
	 */
	public Salle selectionAleatoire(Labyrinthe laby) {			
		int[] tab = direction(laby, laby.getPositionXActuel(), laby.getPositionYActuel());	
		if(checkArray(laby, laby.getSalles()[tab[0]][tab[1]])) {
			goTo(tab, laby);
			Labyrinthe.setCompteur(Labyrinthe.getCompteur()+1);
			laby.getSalles()[tab[0]][tab[1]].setSalleVisite(true);
			Salle.setNbvisite(Salle.getNbvisite() + 1);
			laby.getSalles()[tab[0]][tab[1]].setNbrevisite(Salle.getNbvisite());
			laby.getPositionPasserDuJoueur().add(laby.getSalles()[tab[0]][tab[1]]);
			laby.setPositionXActuel(tab[0]);
			laby.setPositionYActuel(tab[1]);
			laby.getSalles()[tab[0]][tab[1]].setPosX(tab[0]);
			laby.getSalles()[tab[0]][tab[1]].setPosY(tab[1]);
		}else {
			return null;
		}
		return laby.getSalles()[laby.getPositionXActuel()][laby.getPositionYActuel()];
	}
	
	/**
	 * Controle permettant de vérifier si la salle suivante existe et qu'il n'a pas été visité
	 * @param laby
	 * @param salle la salle actuelle
	 * @return vrai si une salle non visité a été trouvé
	 */
	public boolean checkArray(Labyrinthe laby, Salle salle) {
		for(int i=0;i<laby.getSalles().length;i++) {
			for(int j=0;j<laby.getSalles().length;j++) {
				if(salle.equals(laby.getSalles()[i][j]) && !salle.isSalleVisite()) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * fonction pour générer un chemin aléatoire en visitant toutes les salles
	 * @param laby
	 */
	public void generateLabyRecursive(Labyrinthe laby) { // fonction recursive
		if(selectionAleatoire(laby) == null) {
			if(laby.getPositionPasserDuJoueur().size()>1 && laby.getPositionPasserDuJoueur().get(0) != null) {
				Labyrinthe.getTabCompteur().add(Labyrinthe.getCompteur());
				Labyrinthe.getListPositionSortie().add(laby.getSalles()[laby.getPositionXActuel()][laby.getPositionYActuel()]);
				Labyrinthe.setCompteur(Labyrinthe.getCompteur()-1);
				laby.getPositionPasserDuJoueur().removeLast();
				laby.setPositionXActuel(laby.getPositionPasserDuJoueur().getLast().getPosX());
				laby.setPositionYActuel(laby.getPositionPasserDuJoueur().getLast().getPosY());
				generateLabyRecursive(laby);
			}
			else {
				Labyrinthe.getTabCompteur().add(Labyrinthe.getCompteur());
				Labyrinthe.getListPositionSortie().add(laby.getSalles()[laby.getPositionXActuel()][laby.getPositionYActuel()]);
				return;
			}
		}
		else {
			generateLabyRecursive(laby);
		}
	}
	
	/**
	 * fonction rassemblant les directions nord sud est ouest
	 * @param tabIndice
	 * @param laby
	 */
	public void goTo(int[] tabIndice, Labyrinthe laby) {
		goToNord(tabIndice, laby);
		goToSud(tabIndice, laby);
		goToEst(tabIndice, laby);
		goToWest(tabIndice, laby);
		
	}
	/**
	 * Méthode pour aller au nord et casser un mur
	 * @param tabIndice
	 * @param laby
	 * @return
	 */
	public boolean goToNord(int[] tabIndice, Labyrinthe laby) {
		if(tabIndice[2] == 0) {
			laby.getSalles()[laby.getPositionXActuel()][laby.getPositionYActuel()].setNotMurNord(true); // on casse le mur au nord de la salle actuelle
			laby.getSalles()[tabIndice[0]][tabIndice[1]].setNotMurSud(true); // on casse le mur au sud de la prochaine salle
			return true;
		}
		return false;
	}
	/**
	 * Méthode pour aller au sud et casser un mur
	 * @param tabIndice
	 * @param laby
	 * @return
	 */
	public boolean goToSud(int[] tabIndice, Labyrinthe laby) {
		if(tabIndice[2] == 1) {
			laby.getSalles()[laby.getPositionXActuel()][laby.getPositionYActuel()].setNotMurSud(true); // on casse le mur au sud de la salle actuelle
			laby.getSalles()[tabIndice[0]][tabIndice[1]].setNotMurNord(true); // on casse le mur au nord de la prochaine salle
			return true;
		}
		return false;
	}
	/**
	 * Méthode pour aller à l'est et casser un mur
	 * @param tabIndice
	 * @param laby
	 * @return
	 */
	public boolean goToEst(int[] tabIndice, Labyrinthe laby) {
		if(tabIndice[2] == 2) {
			laby.getSalles()[laby.getPositionXActuel()][laby.getPositionYActuel()].setNotMurEst(true); // on casse le mur à l'est de la salle actuelle
			laby.getSalles()[tabIndice[0]][tabIndice[1]].setNotMurOuest(true); // on casse le mur à l'ouest de la prochaine salle
			return true;
		}
		return false;
	}
	
	/**
	 * Méthode pour aller à l'ouest et casser un mur
	 * @param tabIndice
	 * @param laby
	 * @return
	 */
	public boolean goToWest(int[] tabIndice, Labyrinthe laby) {
		if(tabIndice[2] == 3) { // 
			laby.getSalles()[laby.getPositionXActuel()][laby.getPositionYActuel()].setNotMurOuest(true); // on casse le mur à l'ouest de la salle actuelle
			laby.getSalles()[tabIndice[0]][tabIndice[1]].setNotMurEst(true); // on casse le mur à l'est de la prochaine salle
			return true;
		}
		return false;
	}
	

	/**
	 * Trouver la sortie (chemin le plus long)
	 * @param laby
	 * @return la salle la plus longue
	 */
	public static Salle sortie(Labyrinthe laby) {
		int cheminMax = 0;
		int position = 0;
		int positionTrouve = 0;
		for (Integer nb : Labyrinthe.getTabCompteur()) {
			position++;
			if(cheminMax < nb) {
				cheminMax = nb;
				positionTrouve = position;		
			}
		}		
		Salle salle = Labyrinthe.getListPositionSortie().get(positionTrouve-1);
		return salle;
	}
	/** 
	 * Methode qui initialise les salles du labyrinthes aleatoirement avec les 
	 * monstres et les objets aleatoires 
	 *  
	 * @param labyrinthe : dans le quel se trouvent les salles 
	 * @param servSalle  : pour pouvoir utiliser les services contenus dans la 
	 *                   classe ServiceSalle 
	 * @param servObj    : pour pouvoir utiliser et transmettre les services 
	 *                   contenus dans la classe ServiceObjet 
	 * @param servPotion : pour pouvoir utiliser et transmettre les services 
	 *                   contenus dans la classe ServicePotion 
	 */ 
	public static void generationAleaMonstresObjetsParSalle(Labyrinthe labyrinthe, ServiceSalle servSalle, 
			ServiceObjet servObj, ServicePotion servPotion) { 
		int variableTailleMaxLab = labyrinthe.getLongueur(); 
		int totalMonstres = (variableTailleMaxLab*variableTailleMaxLab)/3; 
		int totalObjet = totalMonstres+2 ; 
		Salle salle; 
		Salle salleSortie = sortie(labyrinthe);
		
		for (int i = 0; i < totalMonstres; i++) { 
			int xAlea = Outils.generationAleaMinMAx(0, variableTailleMaxLab); 
			int yAlea = Outils.generationAleaMinMAx(0, variableTailleMaxLab); 
			salle = labyrinthe.getSalles()[xAlea][yAlea]; 
			
			
		
			if((xAlea!=salleSortie.getPosXsortie())&&(yAlea!=salleSortie.getPosYsortie()) &&!(xAlea==0&&yAlea==0)){
				servSalle.ajoutMonstre(salle, labyrinthe); 
			}
			else {
				i--;
			}
 
		} 
		for (int j = 0; j < totalObjet; j++) { 
 
			int xAlea = Outils.generationAleaMinMAx(0, variableTailleMaxLab);
			int yAlea = Outils.generationAleaMinMAx(0, variableTailleMaxLab);
			salle = labyrinthe.getSalles()[xAlea][yAlea];
			if((xAlea!=salleSortie.getPosXsortie())&&(yAlea!=salleSortie.getPosYsortie()) &&!(xAlea==0&&yAlea==0)){
			servSalle.ajouterObjet(salle, servPotion, servObj); 
			}
			else {
				j--;
			}
		} 
	}
}

