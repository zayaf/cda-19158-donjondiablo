package fr.afpa.services;

import fr.afpa.entites.Labyrinthe;
import fr.afpa.entites.Monstre;
import fr.afpa.entites.Objet;
import fr.afpa.entites.Personnage;
import fr.afpa.entites.Salle;

public class ServiceSalle {
	
	/**
	 * Creation de chaque salle du labyrinthe
	 * @param laby : le labyrinthe généré 
	 */
	public void createSalle(Labyrinthe laby) {
		for(int i =0; i< laby.getLongueur();i++) {
			for(int j=0; j< laby.getLargeur(); j++) {
				laby.getSalles()[i][j] = new Salle(i,j);
				laby.getSalles()[i][j].setPosXsortie(i);
				laby.getSalles()[i][j].setPosYsortie(j);
				
			}
		}
		laby.getPositionPasserDuJoueur().add(laby.getSalles()[0][0]);
	}
	
	/**
	 * permet d'ajouter des monstres dans la salle
	 * 
	 * @param salle : une des salles du labyrinthe 
	 * @param laby : Pour pouvoir recuperer les donnes du labyrinthe et initialiser le monstre à sa creation
	 */
	public void ajoutMonstre(Salle salle, Labyrinthe laby) {
		MonstreService monstreServ = new MonstreService();
		Monstre monstre;
		monstre = monstreServ.generationMonstre(laby);
		salle.getListeMonstres().put(monstre.getIdentifiant(), monstre);

	}

	/**
	 * permet d'ajouter des aléatoirement objets dans la salle
	 * 
	 * @param salle : une des salles du labyrinthe 
	 */
	public void ajouterObjet(Salle salle,ServicePotion potionCree,ServiceObjet serviceObj) {
		
		Objet objet;

		objet = serviceObj.generationObjet(potionCree);
		salle.getListeObjets().put(objet.getIdentifiant(), objet);
		
	}
	
	/**
	 * récuperer la salle du nord selon la position du joueur
	 * cette methodes permet au joeur de regarder ou d'avancer dans cette direction selon les possibilités
	 * @param laby
	 * @param personnage
	 * @return la salle du nord
	 */
	public Salle getSalleNord(Labyrinthe laby, Personnage personnage) {
		return laby.getSalles()[personnage.getPositionX()-1][personnage.getPositionY()];
		
	}
	
	/**
	 * Recuperer la salle du sud selon la position du joueur
	 *  cette methodes permet au joeur de regarder ou d'avancer dans cette direction selon les possibilités
	 * @param laby
	 * @param personnage
	 * @return la salle du sud
	 */
	public Salle getSalleSud(Labyrinthe laby, Personnage personnage) {
		return laby.getSalles()[personnage.getPositionX()+1][laby.getJoueur().getPositionY()];
		}
	
	/**
	 * Recuperer la salle de l'est selon la position du joueur
	 * cette methodes permet au joeur de regarder ou d'avancer dans cette direction selon les possibilités
	 * @param laby
	 * @param personnage
	 * @return la salle de l'est
	 */
	public Salle getSalleEst(Labyrinthe laby, Personnage personnage) {
		return laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()+1];
	}
	
	/**
	 * Recuperer la salle de l'ouest selon la position du joueur
	 * cette methodes permet au joeur de regarder ou d'avancer dans cette direction selon les possibilités
	 * @param laby
	 * @param personnage
	 * @return la salle de l'ouest
	 */
	public Salle getSalleOuest(Labyrinthe laby, Personnage personnage) {
		return laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()-1];
	}
	
	public Salle rechercheSalleActuelle(Labyrinthe laby) {
		return laby.getSalles()[laby.getJoueur().getPositionX()][laby.getJoueur().getPositionY()];
		
	}
}
