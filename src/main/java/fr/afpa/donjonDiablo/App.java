package fr.afpa.donjonDiablo;

import java.util.Scanner;

import fr.afpa.ihm.JoueurIhm;
import fr.afpa.ihm.MenuIhm;
import fr.afpa.ihm.SalleIhm;

import fr.afpa.services.LabyrintheService;
import fr.afpa.services.PersonnageServices;
import fr.afpa.services.ServiceJoueur;
import fr.afpa.services.ServiceObjet;
import fr.afpa.services.ServicePotion;
import fr.afpa.services.ServiceSalle;

public class App {
	public static void main(String[] args) {
		LabyrintheService labServ=new LabyrintheService();
		ServiceSalle servSalle =new ServiceSalle();
		ServiceObjet servObj =new ServiceObjet();
		ServicePotion servPotion =new ServicePotion();
		SalleIhm ihmSalle =new SalleIhm();
		ServiceJoueur servJoueur = new ServiceJoueur();
		Scanner in = new Scanner(System.in);
		PersonnageServices perServ =new PersonnageServices();
		MenuIhm menu = new MenuIhm();
		JoueurIhm ihmJoueur =new JoueurIhm();
	
		
		menu.menuPremiereConnexion(in, labServ, servSalle, servObj, servPotion, ihmJoueur, perServ, ihmSalle, servJoueur);
		
	
	}

}
