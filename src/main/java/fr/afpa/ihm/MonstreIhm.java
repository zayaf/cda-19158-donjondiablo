package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entites.Monstre;

import fr.afpa.entites.Salle;
import fr.afpa.outils.Outils;

public class MonstreIhm {

	
	
	/**
	 * permet d'afficher le ou les monstres de la salle créee dans le labyrinthe. 
	 * Le joueur peur ainsi voir ce qu'il y a dans la salle actuelle ou l'une des salles adjacentes
	 * @param salle : une des salles du labyrinthe
	 */
	public static void afficherMonstre(Salle salle) {
		if (salle.getListeMonstres().size() == 0) {
			System.out.println("Vous avez de la chance ! Il n'y a pas de monstres dans cette salle !");
		} else {
			if (salle.getListeMonstres().size() > 0) {
				for (Object monstre : salle.getListeMonstres().values()) {

					System.out.println((((Monstre) monstre).getIdentifiant()) +" - Ce monstre a " + ((Monstre) monstre).getPointDeVie() + " et a en force : " + ((Monstre) monstre).getPointDeForce() );
				}

			}
		}
	}
	
	/**
	 * permet de renvoyer un mostre lorsque l'identifiant saisi correspond à l'identifiant d'un monstre de la salle
	 * cela permet au joeur de choisir quel monstre attaquer en premier
	 * @param salle : la salle dans laquelle se trouve le joueur 
	 * @param monstre : le monstre à chercher dans la liste de monstre de la salle 
	 * @return le monstre trouvé
	 */

	public static Monstre choixMonstreAttaquer(Scanner in,Salle salle) {
		
		String idMonstre="";
		Monstre monstre=null;
		afficherMonstre(salle);
		System.out.println("\nVeuillez saisir l'identifiant du monstre à attaquer");
		idMonstre = in.nextLine();
		Outils.cls();
		if (salle.getListeMonstres().containsKey(idMonstre)) {
			monstre=salle.getListeMonstres().get(idMonstre);
			return monstre;

		} else {
			System.out.println("\nIl n'y a pas de monstre ayant cet identifiant !\n");
			return null;
		}
	}
}
