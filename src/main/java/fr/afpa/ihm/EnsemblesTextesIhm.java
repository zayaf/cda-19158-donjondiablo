package fr.afpa.ihm;

import fr.afpa.outils.Outils;

public class EnsemblesTextesIhm {

	/**
	 * Textes affichés aleatoirement au tout debut du lancement de l'application
	 */
	public static void premiereConnexion() {
		int alea = Outils.generationAlea(4);
		StringBuilder textAffiche = new StringBuilder();
		if (alea == 1) {

			textAffiche.append(
					"\nUne porte massive se dresse devant vous. N'écoutant que votre bravoure vous l'ouvrez avec puissance. "
							+ "\nUn escalier vers les limbes d'un sombre donjon se présente devant vous. "
							+ "\nSaurez vous trouver le courage de vous aventurer dans les méandres du terrible donjon ?\n"
							+ "\n1 – J'allume ma torche, je trouve le courage, et j'y vais ! (Jouer)\n"
							+ "\n2 – Tiens je n'aurai pas oublié ma tarte dans le four ? (Quitter)\n");

		} else if (alea == 2) {
			textAffiche.append(
					"\nVous vous êtes réveillés dans un vieux château sans aucun souvenir de comment vous en êtes arrivés là."
							+ "\nDans un coin de la pièce vous remarquez une petite trappe en bois et juste à côté, quelqu'un s'est amusé à écrire"
							+ "\navec ce que vous espérez être de la peinture rouge « Do NOT ENTER The Labyrinth ! »"
							+ "\n\n1 - Ouvrir la trappe et sauter dedans, de toute façon vous ne savez pas lire l'anglais. (Jouer)"
							
							+ "\n2 - Se diriger vers la sortie et vous jurer que c'est la dernière fois que vous buvez autant ! (Quitter)");
		} else if (alea == 3) {
			textAffiche
					.append("\nPour une fois que vous décidez de sortir de chez vous et vous promener dans la forêt... "
							+ "\nVous vous êtes perdus. Au bout d'une heure de marche des ruines étranges vous barrent le chemin."
							+ "\n\n1 - Aventurier dans l’âme vous vous armez de votre couteau suisse et entrez dans les ruines.(Jouer)"
					
							+ "\n2 - Vous sortez votre téléphone et avec GoogleMaps trouvez rapidement chemin jusqu'à chez vous.(Quitter)");
		}
		ASCIIIhm.bienvenue();
		System.out.println("\n" + textAffiche);
	}

	/**
	 * Texte pour demander d'entrer la longueur du Labyrinthe
	 */
	public static void demandeTailleLongueur() {
		System.out.println(
				"\nAvant de continuer plus loin, vous décidez qu'il serait prudent de faire un rapide croquis des lieux. ");

	}

	/**
	 * Texte à afficher au debut et à chaque changement de salle et s'il n'y a pas
	 * de monstre
	 */
	public static void menuChoixMultiples() {
		System.out.println("\nDifférentes possibilités s'offrent à vous. Que souhaitez faire ?"
				+ "\n1 - Vous déplacer dans une des salles adjacentes" + "\n2 - Regarder..."
				+ "\n3 - Vous reposer!"+"\n4 - Regarder la map avec les détails (tricheur)"+"\n5 - Generer un nouveau Labyrinthe"+"\nQ - Quitter definitivement le jeu");
	}

	/**
	 * Texte à afficher lorseque le joueur decide de quitter la partie
	 */
	public static void choixQuitter() {
		System.out.println(
				"Finalement, vous décidez que la vie d'aventurier n'est pas votre tasse de thé, vous rangez votre épée et rebroussez le chemin.\n"
						+ "Cela dit, nous savons vous reviendrez arpenter notre labyrinthe un jour.\n"
						+ "Alors... à bientôt. ");
	}

	/**
	 * Texte à afficher quand le joueur entre dans la salle et se retrouve en face
	 * du monstre, etant donnée qu'il ne peut quitter le combat, ni de faire aucune
	 * autre action le menu suivant devra s'afficher une fois avant le combat avec 0
	 * en parametres puis pendant le combat avec 1 en parametre.
	 * 
	 * @param un entier : Si le chiffre entrée en parametre égal à 0 alors c'est la
	 *           premiere rencontre avec le monstre si l'indice est égal à 1 alors
	 *           le texte affiché sera juste 1 - Attaquer 2 -Quitter.
	 */
	public static void texteRencontreMonstre(int indiceQuelTexteAfficher) {
		int alea = Outils.generationAlea(7);
		int aleaAtt = Outils.generationAlea(7);
		StringBuilder monstreRencontre = new StringBuilder();
		monstreRencontre.append("");
		StringBuilder attaquer = new StringBuilder();
		attaquer.append("");
		if (indiceQuelTexteAfficher == 0) {
			switch (alea) {
			case 1:
				monstreRencontre.append(
						"\nUn ogre énorme vous barre le chemin ! Vous n'avez aucun autre choix que d'attaquer avant que le monstre ne vous écrase avec sa massue ! ");
				break;

			case 2:
				monstreRencontre.append("\nUne ombre étrange s'était tapie dans cette salle."
						+ "\nPetit à petit vous sentez des fines tentacules s'enrouler autour de vos chevilles. "
						+ "\nVous n'avez pas envie de savoir ce qu'elle vous réserve par la suite!"
						+ "\nIl faut vous défendre !");
				break;
			case 3:
				monstreRencontre.append(
						"\nA peine vous avez eu le temps de bouger, qu'une une nuée de chauve souris s'abat sur vous ! "
								+ "\nDefendez-vous au lieu de gesticuler dans tous les sens !");
				break;
			case 4:
				monstreRencontre.append(
						"\nUne odeur nauséabonde vous agresse les narines depuis que vous êtes entrés dans la salle."
								+ "\nEn regardant de plus près vous vous rendez compte qu'il s'agit d'un yaourt périmé, laissé à l'abandon dans la salle depuis des années."
								+ "\nVous le voyez même bouger... et se diriger lentement vers vous... "
								+ "\nNe digérant pas le lactose, puis franchement le yaourt est pas beau à voir, vous décidez de l'attaquer.");
				break;
			case 5:
				monstreRencontre.append("\nVous vous retrouvez face à face avec votre pire ennemi. "
						+ "\nLe NullPointeurException vous regarde droit dans les yeux... "
						+ "\nQuand vous étiez petit c'était lui qui hantait vos cauchemars et faisait disparaître vos chaussettes !"
						+ "\nIl est temps de prendre votre revanche !");
				break;
			case 6 : monstreRencontre.append("\nMartine vous attendait tranquillement dans cette salle armée d’une raquette de tennis de table dans une main et de son ordinateur dans l’autre."
					+ "\nVous savez très bien ce que ça veut dire. Elle va casser tout votre programme !\n" + 
					"C’est la guerre !"); 

			}
			System.out.println(monstreRencontre);
		}

		else if (indiceQuelTexteAfficher == 1) {
			switch (aleaAtt) {
			case 1:
				System.out.println("\n1 - Attaquer\n2- Regarder informations");
				break;
			case 2:
				System.out.println(
						"\n1 - Fouillant votre sac, vous ne trouvez rien d'autre que des chaussettes ! Lancez-les !"
								+ "\n2 - Aficher les papiers du monstre et lui montrer les vôtres au passage.");
				break;
			case 3:
				System.out.println("\n1 - Par pur hasard vous trouvez une épée... en plastique... "
						+ "Qui ne tente rien, n'a rien... ATTAQUEZ !\n2- Regarder les statistiques");
				break;
			case 4:
				System.out.println(
						"\n1 - Vous vous emparez d'un petite Astelia qui passait par là et essayez de taper le monstre avec ! \n2- Regarder informations");
				break;
			case 5:
				System.out.println("\n1 - Étant un vrai Mac Guyver du Labyrinthe vous "
						+ "vous fabriquez un tank avec une trombone et roulez sur le monstre !\n2- Regarder les statistiques");
				break;
			case 6:
				System.out.println(
						"\n1 - Vous faites un croche pied au monstre... c'est étrangement efficace...\n2- Regarder informations");
				break;

			}

		}

	}

	public static void afficherMenuChoixStatistiques() {
		System.out.println(
				"\n1 - Afficher les informations de votre personnage \n2 - Afficher les informations du monstre rencontré\nQ-Pour quitter");
	}

}
