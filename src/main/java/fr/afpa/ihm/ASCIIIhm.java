package fr.afpa.ihm;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

public class ASCIIIhm {
	/**
	 * Message affiché lors que le joueur à perdu
	 */
	public static void gameOver() {
		int largeur=113;
		int hauteur=27;
		
		BufferedImage image=new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_RGB);
		Graphics graph =image.getGraphics();
		graph.setFont(new Font("SansSerif",Font.BOLD, 17));
		
		Graphics2D graph2=(Graphics2D)graph;
		
		graph2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		graph2.drawString("GAME OVER", 5, 20);
		for (int y = 0; y < hauteur; y++) {
			StringBuilder constr= new StringBuilder();
			for (int x = 0; x < largeur; x++) {
				constr.append(image.getRGB(x, y)== -16777216? "#":" ");
			}
			System.out.println(constr);
		}
	}
	
	/**
	 * Message affiché lors de la premiere connexion du joueur
	 */
	public static void bienvenue() {
		int largeur=113;
		int hauteur=27;
		
		BufferedImage image=new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_RGB);
		Graphics graph =image.getGraphics();
		graph.setFont(new Font("Serif",Font.BOLD, 20));
		
		Graphics2D graph2=(Graphics2D)graph;
		
		graph2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		graph2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		graph2.drawString("Bienvenue", 5, 20);
		for (int y = 0; y < hauteur; y++) {
			StringBuilder constr= new StringBuilder();
			for (int x = 0; x < largeur; x++) {
				constr.append(image.getRGB(x, y)== -16777216 ? " ":"#");
			}
			System.out.println(constr);
		}
	}
	/**
	 * Message affiché lorsque le joueur gagne
	 */
	public static void victoire() {
		int largeur=113;
		int hauteur=27;
		
		BufferedImage image=new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_RGB);
		Graphics graph =image.getGraphics();
		graph.setFont(new Font("Sans-Serif",Font.BOLD, 20));
		
		Graphics2D graph2=(Graphics2D)graph;
		
		graph2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		graph2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		graph2.drawString(" Victoire ! ", 5, 20);
		for (int y = 0; y < hauteur; y++) {
			StringBuilder constr= new StringBuilder();
			for (int x = 0; x < largeur; x++) {
				constr.append(image.getRGB(x, y)== -16777216 ? " ":"8");
			}
			System.out.println(constr);
		}
	}
	
}
