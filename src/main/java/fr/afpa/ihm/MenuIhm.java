package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controles.SaisieControl;
import fr.afpa.entites.Joueur;
import fr.afpa.entites.Labyrinthe;
import fr.afpa.entites.Personnage;
import fr.afpa.entites.Salle;
import fr.afpa.outils.Outils;
import fr.afpa.services.LabyrintheService;
import fr.afpa.services.MonstreService;
import fr.afpa.services.PersonnageServices;
import fr.afpa.services.ServiceJoueur;
import fr.afpa.services.ServiceObjet;
import fr.afpa.services.ServicePotion;
import fr.afpa.services.ServiceSalle;

public class MenuIhm {
	/**
	 * Le tout premier Menu affiché lors de la connexion
	 * 
	 * @param scanner    : Scanner utilisé et transmis aux methodes imbriqués à
	 *                   l'interieur
	 * @param labServ    :Pour pouvoir utiliser les methodes de la Classe
	 *                   LabyrintheService
	 * @param servSalle: Pour pouvoir utiliser les methodes de la classe
	 *                   ServiceSalle
	 * @param servObj    : Pour pouvoir utiliser les methodes de la classe
	 *                   ServiceObjet
	 * @param servPotion : Pour pouvoir utiliser les methodes de la classe
	 *                   ServicePotion
	 * @param ihmJoueur  : Pour pouvoir utiliser les methodes de la classe JoueurIhm
	 * @param perServ    : Pour pouvoir utiliser les methodes de la classe
	 *                   ServicePersonnage
	 * @param salleIhm   : Pour pouvoir utiliser les methodes de la classe SalleIhm
	 */
	public void menuPremiereConnexion(Scanner scanner, LabyrintheService labServ, ServiceSalle servSalle,
			ServiceObjet servObj, ServicePotion servPotion, JoueurIhm ihmJoueur, PersonnageServices perServ,
			SalleIhm salleIhm, ServiceJoueur servJoueur) {
		boolean quitter = false;
		String choixUtilisateur;
		while (!quitter) {
			EnsemblesTextesIhm.premiereConnexion();
			choixUtilisateur = scanner.nextLine();
			Outils.cls();
			if ("1".equals(choixUtilisateur)) {
				menuChoixTaille(scanner, labServ, servSalle, servObj, servPotion, ihmJoueur, perServ, salleIhm,
						servJoueur);
			} else if ("2".equals(choixUtilisateur)) {
				EnsemblesTextesIhm.choixQuitter();
				quitter = true;
			} else {
				System.out.println(EnsembleAffichageErreur.ERREUR_SAISIE);
			}

		}

	}

	/**
	 * Menu qui s'occupe de demander la taille du labyrinthe au joueur.
	 * 
	 * @param in         : Scanner pour pouvoir le passer dans les methodes
	 *                   imbriqués dans le menu et pourvoir l'utiliser
	 * @param labServ    : Pour pouvoir utiliser les methodes de la Classe :
	 *                   LabyrintheService
	 * @param servSalle  : Pour pouvoir utiliser les methodes de la Classe :
	 *                   ServicesSalle
	 * @param servObj    : Pour pouvoir utiliser les methodes de la Classe :
	 *                   ServiceObjet
	 * @param servPotion : Pour pouvoir utiliser les methodes de la Classe :
	 *                   ServicePotion
	 * @param ihmJoueur  : Pour pouvoir utiliser les methodes de la Classe :
	 *                   JoueurIhm
	 * @param perServ    : Pour pouvoir utiliser les methodes de la Classe :
	 *                   PersonnageService
	 * @param salleIhm   : Pour pouvoir utiliser les methodes de la Classe :
	 *                   SalleIhm
	 */
	public void menuChoixTaille(Scanner in, LabyrintheService labServ, ServiceSalle servSalle, ServiceObjet servObj,
			ServicePotion servPotion, JoueurIhm ihmJoueur, PersonnageServices perServ, SalleIhm salleIhm,
			ServiceJoueur servJoueur) {
		
		String tailleLabyrinthe;
		Labyrinthe laby;
		EnsemblesTextesIhm.demandeTailleLongueur();

		while (true) {
			System.out.println("\nDe quelle taille le labyrinthe devrait être ?" + "\n\nR- Pour quitter definitivement"
					+ "\n\nS- Pour afficher les scores" + "\n\nTaille (minimum 4, maximum 40): ");

			tailleLabyrinthe = in.nextLine();
			Outils.cls();
			if (!"r".equalsIgnoreCase(tailleLabyrinthe) && SaisieControl.saisieNonVide(tailleLabyrinthe)
					&& SaisieControl.saisieTailleLaby(tailleLabyrinthe)) {
				laby = labServ.genereLabyrinthe(Integer.parseInt(tailleLabyrinthe), Integer.parseInt(tailleLabyrinthe),
						servSalle, servObj, servPotion, servJoueur);
				menuMultiChoix(in, laby, servSalle, ihmJoueur, perServ, salleIhm, servPotion, servObj);
			} else if (SaisieControl.saisieNonVide(tailleLabyrinthe) && "r".equalsIgnoreCase(tailleLabyrinthe)) {
				EnsemblesTextesIhm.choixQuitter();
				System.exit(0);
			} else if (SaisieControl.saisieNonVide(tailleLabyrinthe) && "s".equalsIgnoreCase(tailleLabyrinthe)) {
				menuScores(in);
			}

			else {
				System.out.println(
						"\nHmmmm, non, décidément ce croquis ne rend pas justice à la taille de ce labyrinthe, va falloir tout recommencer");
			}
		}
	}
	/**
	 * Menu qui affiche les scores du joueur
	 * @param scanner : pour que le joueur puisse revenir au menu precedent
	 */
	private void menuScores(Scanner scanner) {
		System.out.println("\nPour retourner au menu precedent tappez R ! \n");
		Outils.afficheScore();
		String choix = scanner.nextLine();
		if ("R".equalsIgnoreCase(choix)) {
			Outils.cls();
			return;
		} else {
			System.out.println(EnsembleAffichageErreur.ERREUR_SAISIE);
		}
	}

	/**
	 * Menu de multi choix disponible pour le joueur apres la saisie de la taille du
	 * labyrinthe
	 * 
	 * @param in        : Scanner qui sera utilise dans pour le menu et les methodes
	 *                  imbriqués
	 * @param laby      : Labyrinthe crée apres la demande de la taille
	 * @param servSalle : Pour pouvoir utiliser les methodes de la Classe :
	 *                  ServicesSalle
	 * @param ihmJoueur : Pour pourvoir utiliser l'affichage concernant le joueur
	 * @param perServ   : Pour Utiliser les methodes propres à la classe
	 *                  PersonnageService
	 */
	public void menuMultiChoix(Scanner in, Labyrinthe laby, ServiceSalle servSalle, JoueurIhm ihmJoueur,
			PersonnageServices perServ, SalleIhm salleIhm, ServicePotion servPotions, ServiceObjet servObjet) {
		boolean quitter = false;
		String choixJoueur;

		Personnage joueur = laby.getJoueur();
		while (!quitter) {
			LabyrintheIhm.affichageLabyNormal(laby);
			Salle salle = servSalle.rechercheSalleActuelle(laby);
			// si la salle n'as pas de monstre tous les menus sont affichées
			if (salle.isOut()) {
				ASCIIIhm.victoire();
				Outils.stockerScore((Joueur) joueur);
				System.out.println("Voulez vous rejouer?\n1 - Oui\n2 - Non");
				choixJoueur = in.nextLine();
				Outils.cls();
				if ("1".equals(choixJoueur))
					quitter = true;

				else {
					System.exit(0);
				}
			} else if (salle.getListeMonstres().size() == 0) {
				EnsemblesTextesIhm.menuChoixMultiples();
				choixJoueur = in.nextLine();
				Outils.cls();
				switch (choixJoueur) {
				case "1":
					perServ.menuDirection(laby, joueur, in, servSalle, ihmJoueur, perServ, salleIhm, servPotions,
							servObjet);// deplacer
					MonstreService.deplacementAleatoireMonstre(laby);
					break;

				case "2":
					salleIhm.regarder(laby, joueur, in, servPotions, servObjet);// regarder
					break;

				case "3":
					ihmJoueur.controleRepos((Joueur) joueur);
					break;
				case "4":
					LabyrintheIhm.affichageLabyTriche(laby);
					break;
				case "5":
					quitter = true;// revenir arriere et generer new laby
					break;
				case "Q":
					EnsemblesTextesIhm.choixQuitter();
					System.exit(0);
					break;
				default:
					// si erreur
					System.out.println(EnsembleAffichageErreur.ERREUR_SAISIE);
				}
				// sinon: appel menu choix monstres
			}

			else {
				if (joueur.getPointDeVie() <= 0 && salle.getListeMonstres().size() > 0) {
					System.out.println("Voulez vous rejouer?\n1 - Oui\n2 - Non");
					choixJoueur = in.nextLine();
					Outils.cls();
					if ("1".equals(choixJoueur))
						return;

					else {
						System.exit(0);
					}
					break;
				} else if (joueur.getPointDeVie() > 0 && salle.getListeMonstres().size() > 1) {
					System.out.println("Oh non ! Il y a plusieurs monstres dans cette salle!");
					ihmJoueur.menuChoixMonstre(in, salle, (Joueur) joueur, perServ);
				} else if (joueur.getPointDeVie() > 0 && salle.getListeMonstres().size() == 1) {
					System.out.println("Oh non ! Quelquechose cloche avec cette salle");
					ihmJoueur.menuChoixMonstre(in, salle, (Joueur) joueur, perServ);
				}
				// appele service qui va gerer le choix du monstre et par la suite le combat

			}

		}

	}

}
