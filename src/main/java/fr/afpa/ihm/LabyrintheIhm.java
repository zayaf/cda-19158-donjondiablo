package fr.afpa.ihm;

import fr.afpa.entites.Joueur;
import fr.afpa.entites.Labyrinthe;
import fr.afpa.services.LabyrintheService;

public class LabyrintheIhm {
	/**
	 * Affichage du labyrinthe et les données de la salle
	 * @param laby
	 */
	public static void affichageLabyTriche(Labyrinthe laby) {
		for(int i=0;i<laby.getLongueur();i++) {
			for(int j=0;j<laby.getLargeur();j++) {
				if(!laby.getSalles()[i][j].isNotMurNord()) {
					
					System.out.print("+---------");
				}
				else {
					System.out.print("+         ");
				}
			}
			System.out.println("+");
			for(int j=0;j<laby.getLargeur();j++) {		
				if(laby.getSalles()[i][j].isJoueurPresent()) {
					if(laby.getSalles()[i][j].equals(LabyrintheService.sortie(laby)) && laby.getSalles()[i][j].isNotMurOuest()) {
						if(laby.getSalles()[i][j].getListeMonstres().size()!=0) {
							System.out.print("  "+laby.getSalles()[i][j].getListeMonstres().size()+"M  "+Joueur.getPseudo()+"  S ");
							}
						else if(laby.getSalles()[i][j].getListeObjets().size()!=0){
							System.out.print("  "+laby.getSalles()[i][j].getListeObjets().size()+"O  "+Joueur.getPseudo()+"  S ");
						}
						else {
								System.out.print("   "+Joueur.getPseudo()+"  S   ");
							}
						
					}
					else if(laby.getSalles()[i][j].equals(LabyrintheService.sortie(laby)) && !laby.getSalles()[i][j].isNotMurOuest()) {
						if(laby.getSalles()[i][j].getListeMonstres().size()!=0) {
							System.out.print("| "+laby.getSalles()[i][j].getListeMonstres().size()+"M  "+Joueur.getPseudo()+"  S ");
							}
						else if(laby.getSalles()[i][j].getListeObjets().size()!=0){
							System.out.print("| "+laby.getSalles()[i][j].getListeObjets().size()+"O  "+Joueur.getPseudo()+"  S ");
						}
						else {
								System.out.print("|   "+Joueur.getPseudo()+" S   ");
							}			
					}
					else {	
						if(!laby.getSalles()[i][j].isNotMurOuest()) {
							if(i== 0 && j==0) {
							System.out.print("|   "+Joueur.getPseudo()+" E   ");
							}else {
								if(laby.getSalles()[i][j].getListeMonstres().size()!=0) {
									System.out.print("|  "+laby.getSalles()[i][j].getListeMonstres().size()+"M  "+Joueur.getPseudo()+"  ");
								}
								else if(laby.getSalles()[i][j].getListeObjets().size()!=0){
									System.out.print("| "+laby.getSalles()[i][j].getListeObjets().size()+"O  "+Joueur.getPseudo()+"   ");
								}
								else {
									System.out.print("|    "+Joueur.getPseudo()+"    ");
								}
								
							}
							
						}
						else {
							if(laby.getSalles()[i][j].getListeMonstres().size()!=0) {
								System.out.print("  "+laby.getSalles()[i][j].getListeMonstres().size()+"M  "+Joueur.getPseudo()+"  ");
								}
							else if(laby.getSalles()[i][j].getListeObjets().size()!=0){
								System.out.print("  "+laby.getSalles()[i][j].getListeObjets().size()+"O  "+Joueur.getPseudo()+"   ");
							}
							else {
									System.out.print("    "+Joueur.getPseudo()+"     ");
								}
							
						}
					}
				}
				else {
					if(laby.getSalles()[i][j].equals(LabyrintheService.sortie(laby)) && laby.getSalles()[i][j].isNotMurOuest()) {
						if(laby.getSalles()[i][j].getListeMonstres().size()!=0) {
							System.out.print("  "+laby.getSalles()[i][j].getListeMonstres().size()+"M   S  ");
							}
						else if(laby.getSalles()[i][j].getListeObjets().size()!=0){
							System.out.print("  "+laby.getSalles()[i][j].getListeObjets().size()+"O   S  ");
						}
						else {
								System.out.print("     S    ");
							}
						
					}
					else if(laby.getSalles()[i][j].equals(LabyrintheService.sortie(laby)) && !laby.getSalles()[i][j].isNotMurOuest()) {
						if(laby.getSalles()[i][j].getListeMonstres().size()!=0) {
							System.out.print("| "+laby.getSalles()[i][j].getListeMonstres().size()+"M    S ");
							}
						else if(laby.getSalles()[i][j].getListeObjets().size()!=0){
							System.out.print("| "+laby.getSalles()[i][j].getListeObjets().size()+"O    S ");
						}else {
								System.out.print("|    S    ");
							}
						
					}
					else {	
						if(!laby.getSalles()[i][j].isNotMurOuest()) {
							if(i== 0 && j==0) {
							System.out.print("|    E    ");
							}else {
								if(laby.getSalles()[i][j].getListeMonstres().size()!=0) {
									System.out.print("|   "+laby.getSalles()[i][j].getListeMonstres().size()+"M    ");
									}
								else if(laby.getSalles()[i][j].getListeObjets().size()!=0){
									System.out.print("|   "+laby.getSalles()[i][j].getListeObjets().size()+"O    ");
								}
								else {
										System.out.print("|         ");
									}				
							}
							
						}
						else {
							if(laby.getSalles()[i][j].getListeMonstres().size()!=0) {
								System.out.print("    "+laby.getSalles()[i][j].getListeMonstres().size()+"M    ");
								}
							else if(laby.getSalles()[i][j].getListeObjets().size()!=0){
								System.out.print("    "+laby.getSalles()[i][j].getListeObjets().size()+"O    ");
							}else {
									System.out.print("          ");
								}						
						}
					}
				}
			}
			System.out.println("|");
			
			if(i == laby.getLongueur()-1) {
				for(int j =0; j< laby.getLargeur();j++) {
					if(!laby.getSalles()[i][j].isNotMurSud()) {
						System.out.print("+---------");
					}
				}
				System.out.println("+");
				
			}
		}
		System.out.println("P = votre position, E = entrée, S= sortie du labyrinthe, M = un monstre; O = objets cachés");
		
	}
	
	public static void affichageLabyNormal(Labyrinthe laby) {
		for(int i=0;i<laby.getLongueur();i++) {
			for(int j=0;j<laby.getLargeur();j++) {
				if(!laby.getSalles()[i][j].isNotMurNord()) {
					
					System.out.print("+---------");
				}
				else {
					System.out.print("+         ");
				}
			}
			System.out.println("+");
			for(int j=0;j<laby.getLargeur();j++) {	
				if(laby.getSalles()[i][j].isJoueurPresent()) {
					if(laby.getSalles()[i][j].isNotMurOuest()) {
						System.out.print("    "+Joueur.getPseudo()+"     ");
					}else {
						System.out.print("|   "+Joueur.getPseudo()+"     ");
					}
				}
				else {
					if(laby.getSalles()[i][j].isNotMurOuest()) {
						System.out.print("          ");
					}else {
						System.out.print("|         ");
					}
				}

			}
			System.out.println("|");
			
			if(i == laby.getLongueur()-1) {
				for(int j =0; j< laby.getLargeur();j++) {
					if(!laby.getSalles()[i][j].isNotMurSud()) {
						System.out.print("+---------");
					}
				}
				System.out.println("+");
			}
		}
		System.out.println("P = votre position");
		
	}
	
	
}
