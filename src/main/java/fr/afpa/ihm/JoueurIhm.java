package fr.afpa.ihm;

import java.util.Map;
import java.util.Scanner;

import fr.afpa.entites.Joueur;
import fr.afpa.entites.Labyrinthe;
import fr.afpa.entites.Monstre;
import fr.afpa.entites.Personnage;
import fr.afpa.entites.Salle;
import fr.afpa.outils.Outils;
import fr.afpa.services.PersonnageServices;
import fr.afpa.services.ServiceJoueur;

public class JoueurIhm {

	/**
	 * Affichage de l'inscription du joueur
	 * @param laby
	 * @param servJoueur
	 * @return un joueur
	 */
	  public static Joueur createJoueur(Labyrinthe laby, ServiceJoueur servJoueur){
		  Scanner in = new Scanner(System.in);
		  System.out.println("Veuillez saisir le nom du joueur"); 
		  String nom = in.nextLine(); 
		  Joueur joueur = servJoueur.createJoueur(laby, nom); 
		  return joueur; 
	  }
	

	/**
	 * permet d'afficher les infos du joueur ou du monstre en fonction de son
	 * instance
	 * 
	 * @param joueur
	 */
	public static void infoJoueur(Personnage joueur) {

		System.out.println("*************************************");

		if (joueur instanceof Monstre) {
			System.out.println("**  Les infos du monstre rencontré:**");
		} else if (joueur instanceof Joueur) {
			System.out.println("**  Les infos de votre personnage: **");
		}
		System.out.println("**  Ses ponts de vie :" + joueur.getPointDeVie() + "           **");
		System.out.println("**  Ses points de force :" + joueur.getPointDeForce() + "         **");
		if (joueur instanceof Joueur) {
			System.out.println("**  Sa bourse : " + ((Joueur) joueur).getOr() + " pièces d'or     **");
		}
		System.out.println("************************************* ");
	}

	/**
	 * Methode qui gere le menu d'affichage pour afficher les points de vie du
	 * joueur et du monstre pendant le combat
	 * 
	 * @param in      : Scanner passe en parametre depuis le menu de connexion pour
	 *                la saisie.
	 * @param joueur  : Le joueur du quel on souhaite afficher les informations
	 * @param monstre : Le monstre pour pouvoir afficher les informations de
	 *                celui-ce si l'utilisateur le souihaite
	 */
	private void menuRegarderStatistique(Scanner in, Joueur joueur, Monstre monstre) {
		boolean quitter = false;
		String choix = "";
		while (!quitter) {
			EnsemblesTextesIhm.afficherMenuChoixStatistiques();
			choix = in.nextLine();
			if (choix.equals("1")) {

				infoJoueur(joueur);

			} else if (choix.equals("2")) {
				infoJoueur(monstre);
			} else if (choix.equals("Q")) {
				quitter = true;
			} else {
				System.out.println(EnsembleAffichageErreur.ERREUR_SAISIE);
			}
		}

	}

	/**
	 * Methode lors de la rencontre avec un monstre
	 * 
	 * @param salle   : Necessaire pour retirer le monstre de la liste des monstres
	 *                de la salle une fois celui ci vaincu
	 * @param joueur  : Pour pouvoir effectuer des modification sur le joueur en
	 *                question
	 * @param monstre : Pour pouvoir recuperer ses points de vie et autres attributs
	 *                et effectuer les modifications dessus
	 * @param perServ Pour utiliser les Services qui se trouvent dans la classe du
	 *                PersonnageServices
	 * @param scanner : Pour la saisie à faire lors des combats.
	 * @return boolean : vrai si le joueur encore en vie apres le combat et le
	 *         monstre mort, faus si le joueur est mort
	 */
	private void rencontreMonstre(Salle salle, Joueur joueur, Monstre monstre, PersonnageServices perServ,
			Scanner scanner) {
		EnsemblesTextesIhm.texteRencontreMonstre(0);
		String choix = "";
		while (monstre.getPointDeVie() > 0 || joueur.getPointDeVie() > 0) {
			EnsemblesTextesIhm.texteRencontreMonstre(1);
			choix = scanner.nextLine();
			Outils.cls();
			if (choix.equals("1")) {

				System.out.println("Le monstre a perdu " + perServ.attaquer(joueur, monstre) + " points de vie!");
				if (monstre.getPointDeVie() > 0) {
					try {
						Thread.sleep(1000);
						System.out.println("Attention le monstre contre-attaque !");
						Thread.sleep(1000);
						System.out.println("Vous avez perdu " + perServ.attaquer(monstre, joueur) + " points de vie");

					} catch (InterruptedException e) {
						System.out.println("Erreur!");
					}

				} else if (joueur.getPointDeVie() <= 0) {
					ASCIIIhm.gameOver();
					break;

				} else if (monstre.getPointDeVie() <= 0) {
					int tempOr=Outils.generationAleaMinMAx(5, 15);
					joueur.setOr(joueur.getOr()+tempOr);
					System.out.println(
							"\nVICTOIRE! Vous avez vaincu le monstre! Vous ramassez "+tempOr+" pieces d'or sur le corps du monstre.");
					salle.getListeMonstres().remove(monstre.getIdentifiant());
					break;

				} else {
					System.out.println(EnsembleAffichageErreur.ERREUR_SAISIE);
				}

			} else if (choix.equals("2")) {
				menuRegarderStatistique(scanner, joueur, monstre);
			}

			else {
				System.out.println(EnsembleAffichageErreur.ERREUR_SAISIE);
			}
		}
	}

	/**
	 * Menu qui permet au joueur de faire le choix de quel monstre il vaut attaquer
	 * s'il rencontre plusieurs monstres
	 * 
	 * @param in      : scanner pour recuperer le choix de l'utilisateur
	 * @param salle   : la salle dans la quel le joueur a rencontre le monstre
	 * @param joueur  : le joueur pour le passer en parametre à la methode imbriqué
	 *                à ce menu
	 * @param perServ : Pour pouvoir utiliser les methodes compris dans la classe
	 *                PersonnageServices
	 */
	public void menuChoixMonstre(Scanner in, Salle salle, Joueur joueur, PersonnageServices perServ) {
		Monstre monstre = null;
		if (joueur.getPointDeVie() <= 0) {
			return;
		} else {
			if (salle.getListeMonstres().size() == 1) {

				for (Map.Entry<String, Monstre> monstr : salle.getListeMonstres().entrySet()) {
					monstre = monstr.getValue();
				}
				rencontreMonstre(salle, joueur, monstre, perServ, in);
			} else {
				while (salle.getListeMonstres().size() != 1) {
					monstre = MonstreIhm.choixMonstreAttaquer(in, salle);
					if (monstre != null && joueur.getPointDeVie() > 0) {
						rencontreMonstre(salle, joueur, monstre, perServ, in);
					} else {
						break;
					}
				}

			}
		}
	}

	/**
	 * Methode qui controle le total de repos que le joueur a fait
	 * 
	 * @param joueur : Pour controler le total de repos fait au cours de la partie
	 * @return
	 */
	public boolean controleRepos(Joueur joueur) {
		if (joueur.getCompteurRepos() < 3) {
			choixSeReposer(joueur);
			joueur.setCompteurRepos(joueur.getCompteurRepos() + 1);
			return true;
		} else {
			System.out.println("Le danger guette ! Vous ne pouvez plus vous reposer");
			return false;
		}
	}

	/**
	 * Methode qui gere l'option repos, à chaque fois que le joueur choisit de se
	 * reposer il gagne un point de vie
	 * 
	 * @param joueur : Pour pouvoir apporter les modifications sur le joueur.
	 */
	public void choixSeReposer(Joueur joueur) {
		System.out.println("\nVous vous enroulez dans votre duvet et vous endormez!");

		int temp = 1;

		try {

			System.out.print("\nzZz");
			Thread.sleep(1000);
			System.out.print("\nzZz zZz");
			Thread.sleep(1000);
			System.out.print("\nzZz");
			Thread.sleep(1000);
			System.out.print("\nzZz zZz");
			joueur.setPointDeVie(joueur.getPointDeVie() + temp);
			System.out.println("\nVous avez recuperé " + temp + " points de vie.");
		} catch (InterruptedException e) {
			System.out.println("Erreur");
			e.printStackTrace();
		}

	}
}
