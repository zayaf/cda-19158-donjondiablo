package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controles.SaisieControl;
import fr.afpa.entites.Bandit;
import fr.afpa.entites.BourseOr;
import fr.afpa.entites.Joueur;
import fr.afpa.entites.Objet;
import fr.afpa.entites.PotionForce;
import fr.afpa.entites.PotionVie;
import fr.afpa.entites.Potions;
import fr.afpa.entites.Salle;
import fr.afpa.outils.Outils;
import fr.afpa.services.ServiceObjet;
import fr.afpa.services.ServicePotion;

public class ObjetsIhm {

	/**
	 * Methode d'affichage si le joueur choisit l'option regarder dans la salle.
	 * 
	 * @param salle  : la salle la liste de laquelle on va parcourir pour recuper
	 *               les objets.
	 * @param joueur : pour savoir si le joueur regarde dans la salle même, s'il
	 *               regarde dans la salle même l'option utiliser l'objet doit etre
	 *               disponible
	 */
	public static void afficherObjets(Salle salle, Joueur joueur) {
//TODO
		if (salle.getListeObjets().size() == 0) {
			System.out.println("\nCette salle ne possede aucun objet!");
		} else {
			System.out.println("\nCette salle contient quelques tresors: \n");
			for (Object objet : salle.getListeObjets().values()) {
				if (objet instanceof PotionVie) {
					System.out.println(((PotionVie) objet).getIdentifiant()
							+ (" - Une potion de vie, + " + ((PotionVie) objet).getPointsPotion() + " en vie"));
				} else if (objet instanceof PotionForce) {
					System.out.println(((PotionForce) objet).getIdentifiant() + " - Une potion de force, + "
							+ ((PotionForce) objet).getPointsPotion() + " en force");
				} else if (objet instanceof BourseOr) {
					System.out.println(((BourseOr) objet).getIdentifiant() + " - Une bourse d'or avec "
							+ ((BourseOr) objet).getOrBourse() + " pieces d'or");
				} else if (objet instanceof Bandit) {
					System.out.println(
							((Bandit) objet).getIdentifiant() + " - Un bandit-manchot assis au fond de la salle");
				}

			}

			if (salle.isJoueurPresent()) {
				System.out.println("\nPour utiliser un des objets entrez son numero.");
			}
		}
		System.out.println("\nPour revenir en arriere tappez: R");
	}

	/**
	 * Methode d'assemblage pour pouvoir utiliser les potions
	 * 
	 * @param scanner     : pour recuperer le choix de l'utilisateur
	 * @param servPotions : Pour pouvoir appeler les services contenus dans la
	 *                    classe ServicePotions
	 * @param servObjet   : Pour pouvoir appeler les services contenus dans la
	 *                    classe ServicesObjets
	 * @param salle       : Necessaire pour savoir s'il faut afficher et rendre
	 *                    disponibles certains choix en fonction si le joueur est
	 *                    present dans la salle ou pas
	 * @param joueur      : joueur sur le quel seront effectués les changement
	 *                    apportés par les objets.
	 */
	public static void utiliserObjet(Scanner scanner, ServicePotion servPotions, ServiceObjet servObjet, Salle salle,
			Joueur joueur) {
		// TODO
		String choix = "";
		boolean quitter = false;
		Objet objet = null;
		Potions potion = null;
		int variableTemp = 0;
		while (!quitter) { 
			afficherObjets(salle, joueur);
			choix = scanner.nextLine();
			Outils.cls();
			if (!choix.equals("R") && SaisieControl.saisieNonVide(choix)&&salle.getListeMonstres().size()==0) {

				if (salle.isJoueurPresent() && salle.getListeObjets().get(choix) != null) {
					objet = salle.getListeObjets().get(choix);
					if (objet instanceof Potions) {
						variableTemp = servPotions.utiliserPotion(((Potions) objet), joueur);
						PotionsIhm.afficherPointRecuperes(((Potions) objet));
						salle.getListeObjets().remove(choix);
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
							System.out.println("Erreur");
						}
						Outils.cls();
						quitter = true;
						break;
					} else if (objet instanceof BourseOr) {
						variableTemp = servObjet.ramasserBourse(joueur, ((BourseOr) objet));
						if (variableTemp != -1) {
							System.out.println("Vous avez gagné " + variableTemp + " pieces d'or!");
							salle.getListeObjets().remove(choix);
							try {
								Thread.sleep(5000);
							} catch (InterruptedException e) {
								System.out.println("Erreur");
							}
							Outils.cls();
							quitter = true;
							break;
						} else
							System.out.println("Erreur");
					} else if (objet instanceof Bandit) {
						potion = servObjet.utilisbanditManchot(joueur, ((Bandit) objet), servPotions);
						if (potion != null) {
							System.out.println("Vous avez echangé " + ((Bandit) objet).getOrDemande()
									+ " pieces d'or auprés du Bandit contre une potion!\n");
							PotionsIhm.afficherTypePotion(potion);
							servPotions.utiliserPotion(potion, joueur);
							PotionsIhm.afficherPointRecuperes(potion);
							salle.getListeObjets().remove(choix);
							try {
								Thread.sleep(5000);
							} catch (InterruptedException e) {
								System.out.println("Erreur");
							}
							Outils.cls();
							quitter = true;
						} else {
							System.out.println(
									"Malheureusement, vous n'avez pas eu assez d'or pour l'echanger avec le bandit contre une potion\n"
											+ "Desinteressé il vous laisse tout seul dans le labyrinthe");
							salle.getListeObjets().remove(choix);
							try {
								Thread.sleep(5000);
							} catch (InterruptedException e) {
								System.out.println("Erreur");
							}
							Outils.cls();
							quitter = true;
						}
					}
				} else {
					System.out.println(EnsembleAffichageErreur.ERREUR_SAISIE);
				}
			} else if (choix.equalsIgnoreCase("R")) {
				Outils.cls();
				quitter = true;
				break;
			} else {
				System.out.println(EnsembleAffichageErreur.ERREUR_SAISIE);
			}

		}

	}
	
	
	
	
	
}
