package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entites.Joueur;
import fr.afpa.entites.Labyrinthe;
import fr.afpa.entites.Personnage;
import fr.afpa.entites.Salle;
import fr.afpa.outils.Outils;
import fr.afpa.services.ServiceObjet;
import fr.afpa.services.ServicePotion;

public class SalleIhm {

	/**
	 * permet de regarder dans la salle : affiche la liste des monstres et des
	 * objets
	 * 
	 * @param salle
	 */
	public void regarderSalleActuelle(Labyrinthe laby, Scanner scanner, ServicePotion servPotions,
			ServiceObjet servObjet) {
		Salle salle = laby.getSalles()[laby.getJoueur().getPositionX()][laby.getJoueur().getPositionY()];
		Personnage joueur = laby.getJoueur();

		ObjetsIhm.utiliserObjet(scanner, servPotions, servObjet, salle, ((Joueur) joueur));
	}

	/**
	 * Affiche les directions possible où le joueur peut regarder
	 * 
	 * @param salle : l'une des salles adjacentes à celle où se trouve le joueur
	 */
	public void choixDirection(Salle salle) {

		System.out.println("Quelle direction voulez-vous regarder?");

		if (salle.isJoueurPresent() && salle.isNotMurNord()) {
			System.out.println("1-Nord");
		}
		if (salle.isJoueurPresent() && salle.isNotMurSud()) {
			System.out.println("2-Sud");
		}
		if (salle.isJoueurPresent() && salle.isNotMurEst()) {
			System.out.println("3-Est");
		}
		if (salle.isJoueurPresent() && salle.isNotMurOuest()) {
			System.out.println("4-Ouest");
		}
		System.out.println("5-Salle actuelle");
		System.out.println("6-Infos joueur");
		System.out.println("7-Retour");
	}

	
	/**
	 * permet au joeur de regarder dans une des alles de son choix. Cette option lui affiche la liste des monstres et des objets se trouvant dans la salle 
	 * @param laby : le labyrinthe où se trouve le joeur 
	 * @param personnage : le joueur 
	 * @param in : le scanner 
	 * @param servPotions : le service qui affiche la liste des potions dans la salle s'il y en a 
	 * @param servObjet : le service qui affichhe la liste des potions dans la salle s'il y en a 
	 */
	public void regarder(Labyrinthe laby, Personnage personnage, Scanner in, ServicePotion servPotions,
			ServiceObjet servObjet) {
		String choix = "";
		while (!"7".equals(choix)) {
			choixDirection(laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()]);

			choix = in.nextLine();
			Outils.cls();
			LabyrintheIhm.affichageLabyNormal(laby);
			switch (choix) {
 
			case "1":
				if (laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].isNotMurNord()) {
					MonstreIhm.afficherMonstre(
							laby.getSalles()[personnage.getPositionX() - 1][personnage.getPositionY()]);

					ObjetsIhm.utiliserObjet(in, servPotions, servObjet,
							laby.getSalles()[personnage.getPositionX() - 1][personnage.getPositionY()],
							((Joueur) personnage));
				}
				break;

			case "2":
				if (laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].isNotMurSud()) {

					MonstreIhm.afficherMonstre(
							laby.getSalles()[laby.getJoueur().getPositionX() + 1][laby.getJoueur().getPositionY()]);
					ObjetsIhm.utiliserObjet(in, servPotions, servObjet,
							laby.getSalles()[laby.getJoueur().getPositionX() + 1][laby.getJoueur().getPositionY()],
							((Joueur) personnage));
				}
				break;

			case "3":
				if (laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].isNotMurEst()) {
					MonstreIhm.afficherMonstre(
							laby.getSalles()[laby.getJoueur().getPositionX()][laby.getJoueur().getPositionY() + 1]);
					ObjetsIhm.utiliserObjet(in, servPotions, servObjet,
							laby.getSalles()[laby.getJoueur().getPositionX()][laby.getJoueur().getPositionY() + 1],
							((Joueur) personnage));
				}
				break;
			case "4":
				if (laby.getSalles()[personnage.getPositionX()][personnage.getPositionY()].isNotMurOuest()) {

					MonstreIhm.afficherMonstre(
							laby.getSalles()[laby.getJoueur().getPositionX()][laby.getJoueur().getPositionY() - 1]);
					ObjetsIhm.utiliserObjet(in, servPotions, servObjet,
							laby.getSalles()[laby.getJoueur().getPositionX()][laby.getJoueur().getPositionY() - 1],
							((Joueur) personnage));
				}
				break;

			case "5":
				regarderSalleActuelle(laby, in, servPotions, servObjet);
				break;
			case "6":
				JoueurIhm.infoJoueur(personnage);
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					System.out.println("Erreur");
				}
				Outils.cls();
				break;
			case "7":
				Outils.cls();
				break;

			default:
				System.out.println("Erreur !");
			}

		}
	}
}
