package fr.afpa.ihm;

import fr.afpa.entites.PotionForce;
import fr.afpa.entites.PotionVie;
import fr.afpa.entites.Potions;

public class PotionsIhm {
	/**
	 * Methode qui en fonction de la potion va afficher un texte different
	 * 
	 * @param potion : La potion pour faire le choix sur le type d'affichage faire
	 */
	public static void afficherTypePotion(Potions potion) {
		if (potion instanceof PotionVie) {
			System.out.println("Vous avez recuperé une potion de vie");
		} else if (potion instanceof PotionForce) {
			System.out.println("Vous avez recuperé une potion de force");
		}
	}

	/**
	 * Methode qui va afficher les points gagnés en fonction de
	 * 
	 * @param potion : pour afficher le texte sur les points que le personnage à
	 *               gagnée en utilisant la potion
	 */
	public static void afficherPointRecuperes(Potions potion) {
		StringBuilder phraseAffichee = new StringBuilder();
		phraseAffichee.append("\nVous avez gagné " + potion.getPointsPotion() + " points");
		if (potion instanceof PotionVie) {
			System.out.println(phraseAffichee + " de vie");
		} else if (potion instanceof PotionForce) {
			System.out.println(phraseAffichee + " de force");
		}
	}
}
