package fr.afpa.entites;

import java.util.Arrays;
import java.util.LinkedList;

public class Labyrinthe {
	private int longueur;
	private int largeur;
	private Salle[][] salles;
	private int positionXActuel;
	private int positionYActuel;
	private LinkedList<Salle> positionPasserDuJoueur;
	private Personnage joueur;
	private static int compteur;
	private static LinkedList<Integer> listCompteur = new LinkedList<Integer>();
	private static LinkedList<Salle> listPositionSortie = new LinkedList<Salle>();
	
	public int getLongueur() {
		return longueur;
	}

	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}

	public int getLargeur() {
		return largeur;
	}

	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	public Salle[][] getSalles() {
		return salles;
	}

	public void setSalles(Salle[][] salles) {
		this.salles = salles;
	}

	public int getPositionXActuel() {
		return positionXActuel;
	}

	public void setPositionXActuel(int positionXActuel) {
		this.positionXActuel = positionXActuel;
	}

	public int getPositionYActuel() {
		return positionYActuel;
	}

	public void setPositionYActuel(int positionYActuel) {
		this.positionYActuel = positionYActuel;
	}

	public LinkedList<Salle> getPositionPasserDuJoueur() {
		return positionPasserDuJoueur;
	}

	public void setPositionPasserDuJoueur(LinkedList<Salle> positionPasserDuJoueur) {
		this.positionPasserDuJoueur = positionPasserDuJoueur;
	}

	public Personnage getJoueur() {
		return joueur;
	}

	public void setJoueur(Personnage joueur) {
		this.joueur = joueur;
	}


	public static int getCompteur() {
		return compteur;
	}

	public static void setCompteur(int compteur) {
		Labyrinthe.compteur = compteur;
	}


	public static LinkedList<Integer> getTabCompteur() {
		return listCompteur;
	}

	public static void setTabCompteur(LinkedList<Integer> tabCompteur) {
		Labyrinthe.listCompteur = tabCompteur;
	}

	public static LinkedList<Salle> getListPositionSortie() {
		return listPositionSortie;
	}

	public static void setListPositionSortie(LinkedList<Salle> listPositionSortie) {
		Labyrinthe.listPositionSortie = listPositionSortie;
	}

	public Labyrinthe(int longueur, int largeur) {
		this.longueur = longueur;
		this.largeur = largeur;
		this.salles = new Salle[this.longueur][this.largeur];
		this.positionXActuel = 0;
		this.positionYActuel = 0;
		
		this.positionPasserDuJoueur = new LinkedList<Salle>();

		joueur = new Joueur(this.longueur*5, this.longueur*2, 5, "Joueur");

		//joueur = new Joueur(this.longueur*5, this.longueur*2, 5, "Joueur");
		joueur.setPositionX(0);
		joueur.setPositionY(0);
	}

	public Labyrinthe() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Labyrinthe [longueur=" + longueur + ", largeur=" + largeur + ", salles=" + Arrays.toString(salles)
				+ ", positionXActuel=" + positionXActuel + ", positionYActuel=" + positionYActuel
				+ ", positionPasserDuJoueur=" + positionPasserDuJoueur + ", joueur=" + joueur + "]";
	}
	
	
	
	
}
