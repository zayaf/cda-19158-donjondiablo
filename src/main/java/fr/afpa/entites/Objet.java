package fr.afpa.entites;

public abstract class Objet {
	private String identifiant;
	private boolean existe;
	private static int autoIncrem;

	public Objet() {
		this.identifiant = ""+ (++autoIncrem);
		this.existe = true;
	}

//Get et les Sets
	public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	public boolean isExiste() {
		return existe;
	}

	public void setExiste(boolean existe) {
		this.existe = existe;
	}

}
