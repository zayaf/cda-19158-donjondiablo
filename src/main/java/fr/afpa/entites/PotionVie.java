package fr.afpa.entites;

import fr.afpa.outils.Outils;

public class PotionVie extends Potions {

	public PotionVie() {
		super();
		this.setPointsPotion(Outils.generationAleaMinMAx(5, 21));
		this.setIdentifiant(getIdentifiant());
	}

	@Override
	public String toString() {
		return "Potion de vie";
	}

}
