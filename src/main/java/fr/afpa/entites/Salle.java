package fr.afpa.entites;

import java.util.HashMap;
import java.util.Map;

public class Salle {

	private boolean murNord;
	private boolean murSud;
	private boolean murEst;
	private boolean murOuest;
	private boolean joueurPresent = false;
	private boolean salleVisite;
	private boolean sortie;
	private int posX;
	private int posY;
	private int posXsortie;
	private int posYsortie;
	private int nbrevisite;
	private static int nbvisite;
	private Map<String, Objet> listeObjets;
	private Map<String, Monstre> listeMonstres;

	public Salle(boolean murNord, boolean murSud, boolean murEst, boolean murOuest, boolean joueurPresent,
			boolean salleVisite, int posX, int posY) {
		super();
		this.murNord = murNord;
		this.murSud = murSud;
		this.murEst = murEst;
		this.murOuest = murOuest;
		this.joueurPresent = joueurPresent;
		this.salleVisite = salleVisite;
		this.posX = posX;
		this.posY = posY;
		
		this.listeObjets = new HashMap<String, Objet>();
		this.listeMonstres = new HashMap<String, Monstre>();
	}

	public Salle() {
		super();
		this.listeObjets = new HashMap<String, Objet>();
		this.listeMonstres = new HashMap<String, Monstre>();
	}
	
	public Salle(int posX, int posY) {
		this.posX = posX;
		this.posY = posY;
		this.listeObjets = new HashMap<String, Objet>();
		this.listeMonstres = new HashMap<String, Monstre>();
	}

	public boolean isNotMurNord() {
		return murNord;
	}

	public void setNotMurNord(boolean murNord) {
		this.murNord = murNord;
	}

	public boolean isNotMurSud() {
		return murSud;
	}

	public void setNotMurSud(boolean murSud) {
		this.murSud = murSud;
	}

	public boolean isNotMurEst() {
		return murEst;
	}

	public void setNotMurEst(boolean murEst) {
		this.murEst = murEst;
	}

	public boolean isNotMurOuest() {
		return murOuest;
	}

	public void setNotMurOuest(boolean murOuest) {
		this.murOuest = murOuest;
	}

	public boolean isJoueurPresent() {
		return joueurPresent;
	}

	public void setJoueurPresent(boolean joueurPresent) {
		this.joueurPresent = joueurPresent;
	}

	public boolean isSalleVisite() {
		return salleVisite;
	}

	public void setSalleVisite(boolean salleVisite) {
		this.salleVisite = salleVisite;
	}
	
	public boolean isOut() {
		return sortie;
	}

	public void setOut(boolean sortie) {
		this.sortie = sortie;
	}


	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public static int getNbvisite() {
		return nbvisite;
	}

	public static void setNbvisite(int nbvisite) {
		Salle.nbvisite = nbvisite;
	}

	public int getNbrevisite() {
		return nbrevisite;
	}

	public void setNbrevisite(int nbrevisite) {
		this.nbrevisite = nbrevisite;
	}

	public Map<String, Objet> getListeObjets() {
		return listeObjets;
	}

	public void setListeObjets(Map<String, Objet> listeObjets) {
		this.listeObjets = listeObjets;
	}

	public Map<String, Monstre> getListeMonstres() {
		return listeMonstres;
	}

	public void setListeMonstres(Map<String, Monstre> listeMonstres) {
		this.listeMonstres = listeMonstres;
	}

	public int getPosXsortie() {
		return posXsortie;
	}

	public void setPosXsortie(int posXsortie) {
		this.posXsortie = posXsortie;
	}

	public int getPosYsortie() {
		return posYsortie;
	}

	public void setPosYsortie(int posYsortie) {
		this.posYsortie = posYsortie;
	}

	@Override
	public String toString() {
		return "Salle [murNord=" + murNord + ", murSud=" + murSud + ", murEst=" + murEst + ", murOuest=" + murOuest
				+ ", joueurPresent=" + joueurPresent + ", salleVisite=" + salleVisite + ", sortie=" + sortie + ", posX="
				+ posX + ", posY=" + posY + ", nbrevisite=" + nbrevisite + ", listeObjets=" + listeObjets
				+ ", listeMonstres=" + listeMonstres + "]";
	}

	

}