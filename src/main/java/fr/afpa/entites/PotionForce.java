package fr.afpa.entites;

import fr.afpa.outils.Outils;

public class PotionForce extends Potions {

	public PotionForce() {
		super();
		this.setPointsPotion(Outils.generationAlea(6));
		this.setIdentifiant(getIdentifiant());
	}

	@Override
	public String toString() {
		return "Potion de Force";
	}

}
