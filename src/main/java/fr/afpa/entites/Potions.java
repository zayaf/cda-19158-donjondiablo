package fr.afpa.entites;

public abstract class Potions extends Objet {
	private int pointsPotion;
	
	//constructeur par defaut! 
	public Potions() {
		super();
		}

//gets et les sets
	public void setPointsPotion(int pointsPotion) {
		this.pointsPotion = pointsPotion;
	}

	public int getPointsPotion() {
		return pointsPotion;
	}

}
