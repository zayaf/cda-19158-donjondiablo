package fr.afpa.entites;

public abstract class Personnage {

	private int pointDeVie;
	private int pointDeForce;
	private String identifiant;
	private int positionX;
	private int positionY;
	
	

	private static int incrementPersonnage;

	public Personnage(int pointDeVie, int pointDeForce) {
		super();
		this.pointDeVie = pointDeVie;
		this.pointDeForce = pointDeForce;
		this.identifiant =  ""+(++incrementPersonnage);
	}

	public Personnage() {
		super();
	}

	public int getPointDeVie() {
		return pointDeVie;
	}

	public void setPointDeVie(int pointDeVie) {
		this.pointDeVie = pointDeVie;
	}

	public int getPointDeForce() {
		return pointDeForce;
	}

	public void setPointDeForce(int pointDeForce) {
		this.pointDeForce = pointDeForce;
	}

	public String getIdentifiant() {
		return identifiant;
	}
	

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

	@Override
	public String toString() {
		return "Personnage [pointDeVie=" + pointDeVie + ", pointDeForce=" + pointDeForce + ", identifiant="
				+ identifiant + "]";
	}

	
	
	

}
