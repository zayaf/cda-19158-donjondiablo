package fr.afpa.entites;

import fr.afpa.outils.Outils;

public class BourseOr extends Objet {
	int orBourse;

	public BourseOr() {
		super();
		this.orBourse = Outils.generationAlea(11);
		this.setIdentifiant(getIdentifiant());
	}

	@Override
	public String toString() {
		return "Bourse d'Or";
	}

	public int getOrBourse() {
		return orBourse;
	}

	public void setOrBourse(int orBourse) {
		this.orBourse = orBourse;
	}

}
