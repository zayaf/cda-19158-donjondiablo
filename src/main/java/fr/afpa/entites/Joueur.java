package fr.afpa.entites;

public class Joueur extends Personnage {
	private int compteurRepos=0;
	private int or;
	private String nom;
	private final static String pseudo = "P";

//constructeur par default
	public Joueur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Joueur(int pointDeVie, int pointDeForce, int or, String nom) {
		super(pointDeVie, pointDeForce);
		this.or=or;
		this.nom=nom;
	}

	public int getCompteurRepos() {
		return compteurRepos;
	}

	public void setCompteurRepos(int compteurRepos) {
		this.compteurRepos = compteurRepos;
	}

	public int getOr() {
		return or;
	}

	public void setOr(int or) {
		this.or = or;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public static String getPseudo() {
		return pseudo;
	}

	@Override
	public String toString() {
		return "Joueur : le joueur possede " + or + "pieces d'or" + "\nIdentifiant : " + this.getIdentifiant()
				+ "\nNom : " + this.getNom() + "\nPoints de vie : " + this.getPointDeVie() + "\nPoints de force : "
				+ this.getPointDeForce();

	}

}
