package fr.afpa.entites;

import fr.afpa.outils.Outils;

public class Bandit extends Objet {
	private int orDemande;

	public Bandit() {
		super();
		this.setIdentifiant(getIdentifiant());
		this.orDemande = Outils.generationAlea(15);
	}

	public int getOrDemande() {
		return orDemande;
	}

	public void setOrDemande(int orDemande) {
		this.orDemande = orDemande;
	}

	@Override
	public String toString() {
		return "Bandit-Manchot possede : "+this.orDemande;
	}

}
