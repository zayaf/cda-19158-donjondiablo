package fr.afpa.outils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import fr.afpa.entites.Joueur;
import fr.afpa.entites.Score;

public class Outils {

	/**
	 * Methode pour generer un nombre aleatoire.
	 * 
	 * @param chiffreMaxAExclure : /\ Le zero,Le nombre entré en parametre, et les chiffres
	 *                           superieurs seront exclus de la generation
	 * @return in : un entier genere aleatoirement
	 */
	public static int generationAlea(int chiffreMaxAExclure) {
		Random entierAlea = new Random();
		int n=0;
		while(n==0){
		n= entierAlea.nextInt(chiffreMaxAExclure);
		}
		return n;
	}
	
	/**
	 * Stocker le nom, le score et la date du jour dans un fichier
	 * @param joueur
	 */
	public static void stockerScore(Joueur joueur) {
		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			fw = new FileWriter(System.getenv("DONJON_SCORE")+"Score.txt", true);	
		    bw = new BufferedWriter(fw);
			bw.write(joueur.getNom());
			bw.write(";");
			bw.write(joueur.getOr()+"");
			bw.write(";");
			bw.write(LocalDate.now().toString());
			bw.newLine();
			bw.close();
		}catch(Exception e){
			System.out.println("Erreur "+e);
		}
		
	}
	 
	/**
	 * Lire le fichier contenant le nom, le score et la date du jour
	 * @return un tableau de string pour pouvoir le parser
	 */
	public static LinkedList<String> LectureFichierScore() {
		LinkedList<String> strScore = new LinkedList<>();
		FileReader fr = null;
		BufferedReader br = null;
		try {		
			// fr = new FileReader("C:\\ENV\\Ressources\\Score.txt");
			fr = new FileReader(System.getenv("DONJON_SCORE")+"Score.txt");
			 br = new BufferedReader(fr);		
			while(br.ready()) {
				strScore.add(br.readLine());
			}		
			br.close();
			fr.close();
		}catch(Exception e) {
			System.out.println("Erreur "+e);
		}
		return strScore;
	}
	
	/**
	 * Recuperer les lignes dans une liste de score
	 * @return la liste contenant les scores
	 */
	public static LinkedList<Score> getAllScore() {		
		List<String> strScore = LectureFichierScore();
		LinkedList<Score> listScore = new LinkedList<Score>();
		String[] splitScore = null;
		for (String score : strScore) {
			splitScore = score.split(";");
			listScore.add(new Score(splitScore[0], Integer.parseInt(splitScore[1]), splitScore[2]));
		}
		Collections.sort(listScore);
		return listScore;
	}
	
	/**
	 * Affichage des 10 meilleurs score;
	 */
	public static void afficheScore() {
		LinkedList<Score> listScore = getAllScore();
		int taille = listScore.size();
		if(taille > 10) {
			taille = 10;
		}
		System.out.println("Joueur\tScore\tDate");
		for(int i=0; i<taille;i++) {
			System.out.println(listScore.get(i).getNom()+ "\t"+listScore.get(i).getScore()+ "\t"+listScore.get(i).getDate() );
		}
	}
	

		
	/**
	 * Methode pour la generation aleatoire en prenant en compte le min et le max
	 * @param min : le minimum voulu inclus
	 * @param max : le max à exclure
	 * @return int: un nombre entier generé aleatoirement
	 */
	public static int generationAleaMinMAx(int min, int max) {
		Random entierAlea = new Random();
		int n=-1;
		while(n<min){
		n= entierAlea.nextInt(max);
		}
		return n;
	}
	
	
	/**
	 * Effacer la console
	 */
	public static void cls() {
		try {
			new ProcessBuilder("cmd","/c","cls").inheritIO().start().waitFor();
		}catch(Exception e) {
			System.out.println(e);
		}
	}
}
