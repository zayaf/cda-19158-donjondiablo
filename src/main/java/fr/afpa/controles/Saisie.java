package fr.afpa.controles;

import java.util.Scanner;

public class Saisie {

	static Scanner in = new Scanner(System.in);

	/**
	 * permet la saisie du nom du joueur
	 * 
	 * @return le nom saisi
	 */
	public static String saisieNom() {

		SaisieControl test = new SaisieControl();

		System.out.println("Veuillez saisir nom");

		String nom = in.nextLine();

		while (test.saisieNonVide(nom) == false) {

			System.out.println(" Erreur ! ");
			return saisieNom();
		}
		return nom;

	}
	
	
}
