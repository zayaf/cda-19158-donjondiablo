package fr.afpa.controles;

public class SaisieControl {

	/**
	 * permet de contoler que le mot saisi ne corespond pas � une chaine vide
	 * 
	 * @param mot
	 * @return true si le mot n'est pas vide et false si c'est le contraire
	 */

	public static boolean saisieNonVide(String mot) {

		if (mot.isEmpty() || mot.matches(" ")) {
			return false;
		}

		else {
			return true;
		}

	}

	/**
	 * permet de controler la saisie de la taille minimum du labyrinthe
	 * 
	 * @param tailleMin
	 * @return true si la taille est superieure � 3 et inferieure à 40 et flase si
	 *         elle est inf�rieur ou egale � 3 et superieure à 40
	 */
	public static boolean saisieTailleLaby(String tailleLaby) {
		try {
			Integer.parseInt(tailleLaby);
		} catch (NumberFormatException e) {
			return false;
		}
		if (Integer.parseInt(tailleLaby) < 3 || Integer.parseInt(tailleLaby) > 40) {
			return false;
		}

		else {
			return true;
		}
	}

	/**
	 * permet de verifier que le chiffre saisi est un chiffre
	 * 
	 * @param chiffreSaisi
	 * @return true si c'est un chiffre ou 2 chiffres et false si c'est autre chose
	 */
	public static boolean saisieChiffre(String chiffreSaisi) {

		if (chiffreSaisi.matches("\\d{1,2}")) {
			return true;
		} else {
			return false;
		}
	}

}
